/**
 * Created by urbanmarovt on 09/02/16.
 */

import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';

import {ReceiverPrefsForm} from 'components';

import {create as createReceiverPrefs} from 'redux/modules/models/receivers/preferences/new';

@connect(
  dispatch => bindActionCreators({...routerActions, createReceiverPrefs}, dispatch)
)
export default class ReceiverPrefsNew extends Component {

  static propTypes = {
    createReceiverPrefs: PropTypes.func.isRequired,
    pushState: PropTypes.func,
    params: PropTypes.object,
    location: PropTypes.object
  };

  handleSubmit = (data) => {
    const {params: {receiverId}, createReceiverPrefs} = this.props;
    createReceiverPrefs(receiverId, data).then((res) => {
      // success
      if (res.error) {
        // handle error
      } else {
        // handle success
        this.props.pushState(null, `/receivers/${receiverId}`);
      }
    });
  }

  handleCancel = () => {
    console.log('CANCEL FORM');
  }

  render() {
    console.log(this.props);

    const {location: {state: {channelType}}} = this.props;

    return (
      <div>
        <div className="row wrapper border-bottom white-bg page-heading">
          <div className="col-lg-10">
            <h2>New receiver preferences</h2>
          </div>
          <div className="col-lg-2">

          </div>
        </div>
        <div className="wrapper wrapper-content animated fadeInRight">
          <div className="row">
            <div className="col-lg-12">
              <div className="ibox float-e-margins">
                <div className="ibox-content p-md">
                  <ReceiverPrefsForm initialValues={{channelType: channelType}} onSubmit={this.handleSubmit}
                                     handleCancel={this.handleCancel}/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
