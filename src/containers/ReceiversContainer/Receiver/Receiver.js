/**
 * Created by urbanmarovt on 19/02/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';

import styles from './Receiver.scss';

import {
  load as loadReceiver,
  edit as editReceiver,
  cancelEditing as cancelEditingReceiver,
  clearState as clearReceiverDetails} from 'redux/modules/models/receivers/details';
import {load as loadReceiverPrefs} from 'redux/modules/models/receivers/preferences/list';

import { FormControls, Button, Glyphicon, ButtonToolbar} from 'react-bootstrap';
import {ReceiverForm, ReceiverPrefs} from 'components';

import {refactorReceiverPrefs} from 'utils/refactor';


function fetchDataDeferred(getState, dispatch) {
  const promises = [];
  promises.push(dispatch(loadReceiver(getState())));
  promises.push(dispatch(loadReceiverPrefs(getState())));

  return Promise.all(promises);
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    receiver: state.models.receivers.details,
    receiverPrefs: state.models.receivers.preferences.list.data
  }),
  dispatch => bindActionCreators(
    {...routerActions, editReceiver, cancelEditingReceiver, clearReceiverDetails}, dispatch)
)
export default class Receiver extends Component {

  static propTypes = {
    receiver: PropTypes.object,
    receiverPrefs: PropTypes.array,
    editReceiver: PropTypes.func,
    cancelEditingReceiver: PropTypes.func,
    clearReceiverDetails: PropTypes.func,
    pushState: PropTypes.func,
    createReceiver: PropTypes.func
  }

  componentWillUnmount() {
    const {clearReceiverDetails} = this.props;

    clearReceiverDetails();
  }


  onPrefsCreateClick = (receiverId, channelType) => {
    this.props.pushState({receiverId, channelType}, `/receivers/${receiverId}/preferences/new`);
  };

  handleCancel = () => {
    console.log('CANCEL FORM');
  };

  handleSubmit = (data) => {
    const {createReceiver} = this.props;
    createReceiver(data).then((res) => {
      // success
      if (res.error) {
        // handle error
      } else {
        // handle success
        this.props.pushState(null, `/receivers/${res.result.body.id}`);
      }
    });
  };

  render() {
    const {receiver, receiverPrefs, editReceiver, cancelEditingReceiver} = this.props;

    return (
      <div className="panel-body">
        <div className="row">
          <div className="col-lg-12">
            <div className="ibox float-e-margins">
              <div className="ibox-content p-md">
                <div>
                  {!receiver.editing &&
                  <form className="form-horizontal">
                    <FormControls.Static label="First name" labelClassName="col-xs-2" wrapperClassName="col-xs-10">{receiver.data.firstName}</FormControls.Static>
                    <FormControls.Static label="Last name" labelClassName="col-xs-2" wrapperClassName="col-xs-10">{receiver.data.lastName}</FormControls.Static>
                    <FormControls.Static label="Email" labelClassName="col-xs-2" wrapperClassName="col-xs-10">{receiver.data.email}</FormControls.Static>
                    <FormControls.Static label="Mobile phone" labelClassName="col-xs-2" wrapperClassName="col-xs-10">{receiver.data.mobilePhone}</FormControls.Static>
                    <div className="row">
                      <div className="col-xs-offset-2 col-xs-10">
                        <ButtonToolbar>
                          <Button onClick={editReceiver}><Glyphicon glyph="edit" /> &nbsp;Edit</Button>
                        </ButtonToolbar>
                      </div>
                    </div>
                    <div className="hr-line-dashed"></div>
                    <div className="row">
                      <div className="col-sm-2">
                        <label className={styles.label}>Channel preferences</label>
                      </div>
                      <div className="col-sm-10">
                        <ReceiverPrefs receiverPrefs={refactorReceiverPrefs(receiverPrefs)} receiverId={receiver.data.id}
                                       onCreateClick={this.onPrefsCreateClick}/>
                      </div>
                    </div>
                  </form>
                  }
                  {receiver.editing &&
                    <ReceiverForm onSubmit={this.handleSubmit} handleCancel={cancelEditingReceiver}/>
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
