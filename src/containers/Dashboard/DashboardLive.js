/**
 * Created by rokfortuna on 29/02/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {MessageStatistics} from 'components';
import {LineChart, BarChart} from 'components';
import {loadCountByTime} from 'redux/modules/models/messages/count';
import {onMessageSocket, clearState} from 'redux/modules/views/dashboard';
import config from 'config';


function fetchDataDeferred(getState, dispatch) {
  const promises = [];
  promises.push(dispatch(loadCountByTime()));
  return Promise.all(promises);
}

let webSocket;

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    countByTime: state.models.messages.count.byTime,
    rates: state.views.dashboard.liveStatistics.rates,
    bars: state.views.dashboard.liveStatistics.bars
  }),
  dispatch => bindActionCreators({onMessageSocket, clearState}, dispatch)
)
export default class DashboardLive extends Component {

  static propTypes = {
    countByTime: PropTypes.object,
    rates: PropTypes.array,
    bars: PropTypes.array,
    onMessageSocket: PropTypes.func,
    clearState: PropTypes.func
  };

  componentDidMount() {
    // live graphs
    console.log(config);
    webSocket = new WebSocket(`ws://${config.socketHost}:${config.socketPort}/v1/global`);
    const {onMessageSocket} = this.props;
    webSocket.onmessage = function(event) {
      console.log('SOCKET_MESSAGE GLOBAL');
      const rec = JSON.parse(event.data);
      const rdata = rec.data;
      const rate = rec.rate;

      onMessageSocket({rdata: rdata, rate: rate});

    };
  }

  componentWillUnmount() {
    webSocket.close();
    this.props.clearState();
  }

  render() {
    const {countByTime, rates, bars} = this.props;

    const rateChartData = {
      data: {
        x: 'x',
        columns: rates
      },
      axis: {
        x: {
          type: 'timeseries',
          tick: {
            format: '%H:%M:%S'
          }
        }
      },
      bindto: '#rateChart'
    };

    const barChartData = {
      data: {
        columns: bars,
        type: 'bar'
      },
      bar: {
        width: {
          ratio: 0.5
        }
      },
      axis: {
        x: {
          type: 'category',
          categories: ['EMAIL', 'SMS', 'PUSH NOTIFICATIONS']
        }
      },
      bindto: '#barChart'
    };

    return (
      <div className="panel-body">
        <div className="row">
          <div className="col-lg-3">
            <MessageStatistics title="All time" totalCount={countByTime.allTime.count} clickedCount={countByTime.allTime.delivered} openedCount={countByTime.allTime.opened} />
          </div>
          <div className="col-lg-3">
            <MessageStatistics title="Annual" totalCount={countByTime.annual.count} clickedCount={countByTime.annual.delivered} openedCount={countByTime.annual.opened} />
          </div>
          <div className="col-lg-3">
            <MessageStatistics title="Monthly" totalCount={countByTime.monthly.count} clickedCount={countByTime.monthly.delivered} openedCount={countByTime.monthly.opened} />
          </div>
          <div className="col-lg-3">
            <MessageStatistics title="Today" totalCount={countByTime.today.count} clickedCount={countByTime.today.delivered} openedCount={countByTime.today.opened} />
          </div>
        </div>

        <div className="row">
          <div className="col-lg-12">
            <div className="ibox float-e-margins">
              <div className="ibox-title">
                <h5>Sending rate</h5>
              </div>
              <div className="ibox-content">
                <div className="row">
                  <div className="col-lg-12">
                    <LineChart data={rateChartData} id="rateChart" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-lg-12">
            <div className="ibox float-e-margins">
              <div className="ibox-title">
                <h5>Message distribution</h5>
              </div>
              <div className="ibox-content">
                <div className="row">
                  <div className="col-lg-12">
                    <BarChart data={barChartData} id="barChart" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

}
