import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';

export default class Dashboard extends Component {

  static propTypes = {
    children: PropTypes.object.isRequired,
    location: PropTypes.object
  };

  render() {

    const {location: {pathname}} = this.props;

    return (
      <div className="wrapper wrapper-content">
        <div className="tabs-container">
          <ul role="tablist" className="nav nav-tabs">
            <li role="presentation" className={pathname === '/' && 'active'}>
              <Link to={`/`}>Basic Info</Link>
            </li>
            <li role="presentation" className={pathname === '/history' && 'active'}>
              <Link to={`/history`}>Statistics</Link>
            </li>
          </ul>
          <div className="tab-content">
            <div role="tabpanel" className="tab-pane active">
              {this.props.children}
            </div>
          </div>
        </div>
      </div>
    );
  }
}


/* <div className="row">
 <div className="col-lg-3">
 <div className="ibox float-e-margins">
 <div className="ibox-title">
 <h5>All time</h5>
 </div>
 <div className="ibox-content">
 <h1 className="m-b-sm m-t-none m-l-none m-r-none">36,650</h1>
 <div>
 <div className="stat-percent font-bold text-navy">98.7% <i className="fa fa-check"></i></div>
 <small>Delivered</small>
 </div>
 <div>
 <div className="stat-percent font-bold text-navy">78% <i className="fa fa-check"></i></div>
 <small>Opened</small>
 </div>
 <div>
 <div className="stat-percent font-bold text-warning">1.3% <i className="fa fa-warning"></i></div>
 <small>Undelivered</small>
 </div>
 </div>
 </div>
 </div>
 <div className="col-lg-3">
 <div className="ibox float-e-margins">
 <div className="ibox-title">
 <h5>Annual</h5>
 </div>
 <div className="ibox-content">
 <h1 className="m-b-sm m-t-none m-l-none m-r-none">16,200</h1>
 <div>
 <div className="stat-percent font-bold text-navy">99.2% <i className="fa fa-check"></i></div>
 <small>Delivered</small>
 </div>
 <div>
 <div className="stat-percent font-bold text-navy">68% <i className="fa fa-check"></i></div>
 <small>Opened</small>
 </div>
 <div>
 <div className="stat-percent font-bold text-navy">0.8% <i className="fa fa-check"></i></div>
 <small>Undelivered</small>
 </div>
 </div>
 </div>
 </div>
 <div className="col-lg-3">
 <div className="ibox float-e-margins">
 <div className="ibox-title">
 <h5>Monthly</h5>
 </div>
 <div className="ibox-content">
 <h1 className="m-b-sm m-t-none m-l-none m-r-none">2,200</h1>
 <div>
 <div className="stat-percent font-bold text-navy">96.6% <i className="fa fa-check"></i></div>
 <small>Delivered</small>
 </div>
 <div>
 <div className="stat-percent font-bold text-warning">58% <i className="fa fa-warning"></i></div>
 <small>Opened</small>
 </div>
 <div>
 <div className="stat-percent font-bold text-danger">3.4% <i className="fa fa-times"></i></div>
 <small>Undelivered</small>
 </div>
 </div>
 </div>
 </div>
 <div className="col-lg-3">
 <div className="ibox float-e-margins">
 <div className="ibox-title">
 <h5>Today</h5>
 </div>
 <div className="ibox-content">
 <h1 className="m-b-sm m-t-none m-l-none m-r-none">200</h1>
 <div>
 <div className="stat-percent font-bold text-warning">99.6% <i className="fa fa-warning"></i></div>
 <small>Delivered</small>
 </div>
 <div>
 <div className="stat-percent font-bold text-warning">10% <i className="fa fa-warning"></i></div>
 <small>Opened</small>
 </div>
 <div>
 <div className="stat-percent font-bold text-navy">0.4% <i className="fa fa-check"></i></div>
 <small>Undelivered</small>
 </div>
 </div>
 </div>
 </div>
 </div>
 <div className="row">
 <div className="col-lg-12">
 <div className="ibox float-e-margins">
 <div className="ibox-title">
 <h5>Channels</h5>
 <div className="pull-right">
 <div className="btn-group">
 <button type="button" className="btn btn-xs btn-white">Today</button>
 <button type="button" className="btn btn-xs btn-white active">Monthly</button>
 <button type="button" className="btn btn-xs btn-white">Annual</button>
 </div>
 </div>
 </div>
 <div className="ibox-content">
 <div className="row">
 <div className="col-lg-9">
 <LineChart data={lineChartData} id="lineChart"/>
 </div>
 <div className="col-lg-3">
 <ul className="stat-list">
 <li>
 <h2 className="no-margins">1,346</h2>
 <small>Total SMS in period</small>
 <div className="stat-percent">28% <i className="fa fa-level-up text-navy"></i></div>
 <div className="progress progress-mini">
 <div style={{width: 28 + '%'}} className="progress-bar"></div>
 </div>
 </li>
 <li>
 <h2 className="no-margins ">3,422</h2>
 <small>Total Email in period</small>
 <div className="stat-percent">60% <i className="fa fa-level-up text-navy"></i></div>
 <div className="progress progress-mini">
 <div style={{width: 60 + '%'}} className="progress-bar"></div>
 </div>
 </li>
 <li>
 <h2 className="no-margins ">980</h2>
 <small>Total Notifications in period</small>
 <div className="stat-percent">22% <i className="fa fa-level-up text-navy"></i></div>
 <div className="progress progress-mini">
 <div style={{width: 22 + '%'}} className="progress-bar"></div>
 </div>
 </li>
 </ul>
 </div>
 </div>
 </div>
 </div>
 </div>
 </div>
 <div className="row">
 <div className="col-lg-12">
 <div className="ibox float-e-margins">
 <div className="ibox-title">
 <h5>Last messages</h5>
 <div className="pull-right">
 <div className="btn-group">
 <button type="button" className="btn btn-xs btn-white active">All</button>
 <button type="button" className="btn btn-xs btn-white">Completed</button>
 <button type="button" className="btn btn-xs btn-white">Pending</button>
 <button type="button" className="btn btn-xs btn-white">Undelivered</button>
 </div>
 </div>
 </div>
 <div className="ibox-content">
 <table className="table table-hover no-margins">
 <thead>
 <tr>
 <th>Status</th>
 <th>Date</th>
 <th>Referent</th>
 <th>Campaign</th>
 <th>Subject</th>
 <th>Delivered</th>
 </tr>
 </thead>
 <tbody>
 <tr>
 <td><small>Pending...</small></td>
 <td><i className="fa fa-clock-o"></i> 11:20pm</td>
 <td>Mojca</td>
 <td>Dostava kreditne kartice</td>
 <td>Dostava kreditne kartice</td>
 <td className="text-warning"> <i className="fa fa-level-up"></i> 24% </td>
 </tr>
 <tr>
 <td><span className="label label-danger">Undelivered</span> </td>
 <td><i className="fa fa-clock-o"></i> 10:40am</td>
 <td>Mojca</td>
 <td>Posodobitev uporabniškega računa</td>
 <td>Posodobitev uporabniškega računa</td>
 <td className="text-danger"> <i className="fa fa-level-up"></i> 0% </td>
 </tr>
 <tr>
 <td><small>Pending...</small> </td>
 <td><i className="fa fa-clock-o"></i> 01:30pm</td>
 <td>Ana</td>
 <td>Novoletna voščila</td>
 <td>Novoletno voščilo</td>
 <td className="text-warning"> <i className="fa fa-level-up"></i> 54% </td>
 </tr>
 <tr>
 <td><small>Pending...</small> </td>
 <td><i className="fa fa-clock-o"></i> 02:20pm</td>
 <td>Petra</td>
 <td>Novoletna voščila</td>
 <td>Novoletno voščilo</td>
 <td className="text-warning"> <i className="fa fa-level-up"></i> 12% </td>
 </tr>
 <tr>
 <td><small>Pending...</small> </td>
 <td><i className="fa fa-clock-o"></i> 09:40pm</td>
 <td>Nejc</td>
 <td>Posodobitev uporabniškega računa</td>
 <td>Posodobitev uporabniškega računa</td>
 <td className="text-warning"> <i className="fa fa-level-up"></i> 22% </td>
 </tr>
 <tr>
 <td><span className="label label-primary">Completed</span> </td>
 <td><i className="fa fa-clock-o"></i> 04:10am</td>
 <td>Miha</td>
 <td>Dostava kreditne kartice</td>
 <td>Dostava kreditne kartice</td>
 <td className="text-navy"> <i className="fa fa-level-up"></i> 100% </td>
 </tr>
 <tr>
 <td><small>Pending...</small> </td>
 <td><i className="fa fa-clock-o"></i> 12:08am</td>
 <td>Miha</td>
 <td>Nov uporabnik</td>
 <td>Nov uporabnik</td>
 <td className="text-warning"> <i className="fa fa-level-up"></i> 23% </td>
 </tr>
 </tbody>
 </table>
 </div>
 </div>
 </div>
 </div>

 <div className="wrapper wrapper-content">
 <div className="row">
 <div className="tabs-container">
 <ul className="nav nav-tabs">
 <li className="active"><a data-toggle="tab" href="#tab-live"> Live</a></li>
 <li className=""><a data-toggle="tab" href="#tab-analytics">Analytics</a></li>
 <li className=""><a data-toggle="tab" href="#tab-messages">Messages</a></li>
 </ul>
 <div className="tab-content">
 <div id="tab-live" className="tab-pane active">

 </div>
 <div id="tab-analytics" className="tab-pane">
 <div className="panel-body">

 <div className="row">
 <div className="col-lg-12">
 <div className="ibox float-e-margins">
 <div className="ibox-title">
 <h5>Channels</h5>
 <div className="pull-right">
 <div className="btn-group">
 <button type="button" className="btn btn-xs btn-white">Today</button>
 <button type="button" className="btn btn-xs btn-white active">Monthly</button>
 <button type="button" className="btn btn-xs btn-white">Annual</button>
 </div>
 </div>
 </div>
 <div className="ibox-content">
 <div className="row">
 <div className="col-lg-9">
 <LineChart data={lineChartData} id="lineChart"/>
 </div>
 <div className="col-lg-3">
 <ul className="stat-list">
 <li>
 <h2 className="no-margins">1,346</h2>
 <small>Total SMS in period</small>
 <div className="stat-percent">28% <i className="fa fa-level-up text-navy"></i></div>
 <div className="progress progress-mini">
 <div style={{width: 28 + '%'}} className="progress-bar"></div>
 </div>
 </li>
 <li>
 <h2 className="no-margins ">3,422</h2>
 <small>Total Email in period</small>
 <div className="stat-percent">60% <i className="fa fa-level-up text-navy"></i></div>
 <div className="progress progress-mini">
 <div style={{width: 60 + '%'}} className="progress-bar"></div>
 </div>
 </li>
 <li>
 <h2 className="no-margins ">980</h2>
 <small>Total Notifications in period</small>
 <div className="stat-percent">22% <i className="fa fa-level-up text-navy"></i></div>
 <div className="progress progress-mini">
 <div style={{width: 22 + '%'}} className="progress-bar"></div>
 </div>
 </li>
 </ul>
 </div>
 </div>
 </div>
 </div>
 </div>
 </div>

 </div>
 </div>
 <div id="tab-messages" className="tab-pane">
 <div className="panel-body">
 tab3
 </div>
 </div>
 </div>


 </div>
 </div>
 </div>
 */
