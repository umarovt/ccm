/**
 * Created by urbanmarovt on 03/03/16.
 */
/**
 * Created by urbanmarovt on 19/02/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';
import {
  load as loadMessage,
  clearState as clearMessageDetails} from 'redux/modules/models/messages/details';
import { FormControls} from 'react-bootstrap';
import { DateLabel } from 'components';

function fetchDataDeferred(getState, dispatch) {
  return dispatch(loadMessage(getState()));
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    message: state.models.messages.details.data
  }),
  dispatch => bindActionCreators(
    {...routerActions, clearMessageDetails}, dispatch)
)
export default class Receiver extends Component {

  static propTypes = {
    message: PropTypes.object,
    clearMessageDetails: PropTypes.func
  }

  componentWillUnmount() {
    const {clearMessageDetails} = this.props;

    clearMessageDetails();
  }

  render() {
    const {message} = this.props;

    return (
      <div className="panel-body">
        <div className="row">
          <div className="col-lg-12">
            <div className="ibox float-e-margins">
              <div className="ibox-content p-md">
                <div>
                  <form className="form-horizontal">
                    <FormControls.Static label="Message status" labelClassName="col-xs-2" wrapperClassName="col-xs-10">{message.deliveryStatus}</FormControls.Static>
                    {
                      message.deliveryStatus === 'PENDING' &&
                      <FormControls.Static label="Deliver after" labelClassName="col-xs-2"
                                           wrapperClassName="col-xs-10"><DateLabel date={message.deliverAfter} /></FormControls.Static>
                    }
                    {
                      message.deliveryStatus === 'DELIVERED' &&
                      <FormControls.Static label="Delivar at" labelClassName="col-xs-2"
                                           wrapperClassName="col-xs-10"><DateLabel date={message.deliverAt} /></FormControls.Static>
                    }
                    <FormControls.Static label="Channel" labelClassName="col-xs-2" wrapperClassName="col-xs-10">{message.channelType}</FormControls.Static>
                    <FormControls.Static label="Subject" labelClassName="col-xs-2" wrapperClassName="col-xs-10">{message.subject}</FormControls.Static>
                    { message.channelType !== 'EMAIL' &&
                    <FormControls.Static label="Content" labelClassName="col-xs-2"
                                         wrapperClassName="col-xs-10">{message.content}</FormControls.Static>
                    }
                    { message.channelType === 'EMAIL' &&
                      <div>
                        <FormControls.Static label="Content" labelClassName="col-xs-2"
                                             wrapperClassName="col-xs-10"></FormControls.Static>
                        <div dangerouslySetInnerHTML={{__html: message.content}} />
                      </div>
                    }
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
