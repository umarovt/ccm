/**
 * Created by rokfortuna on 03/03/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';
import { FormControls, Button, Glyphicon, ButtonToolbar} from 'react-bootstrap';
import _ from 'underscore';

import {
  load as loadCampaign,
  edit as editCampaign,
  update as updateCampaign,
  cancelEditing as cancelEditingCampaign,
  clearState as clearCampaignDetails,
  onMessageSocket} from 'redux/modules/models/campaigns/details';
import {isLoaded as isTagsLoaded, load as loadTags} from 'redux/modules/models/tags';
import {TagDisplayForm} from 'components';
import {refactorTags} from 'utils/dataSelectRefactor';
import {DateLabel} from 'components';
import {CampaignForm} from 'components';
import {refactorTags as refactorTagsAPI} from 'utils/refactor';
import {LineChart, BarChart} from 'components';
import config from 'config';

function fetchDataDeferred(getState, dispatch) {
  const promises = [];
  promises.push(dispatch(loadCampaign(getState())));
  if (!isTagsLoaded(getState())) {
    promises.push(dispatch(loadTags()));
  }
  return Promise.all(promises);
}

let webSocket;

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    campaign: state.models.campaigns.details,
    allTags: state.models.tags.data,
    activeTab: state.views.campaign.activeTab,
    rates: state.models.campaigns.details.liveStatistics.rates,
    bars: state.models.campaigns.details.liveStatistics.bars,
    campaignId: state.router.params.campaignId
  }),
  dispatch => bindActionCreators(
    {...routerActions,
      editCampaign,
      updateCampaign,
      cancelEditingCampaign,
      loadCampaign,
      clearCampaignDetails,
      onMessageSocket}, dispatch)
)
export default class Campaign extends Component {

  static propTypes = {
    campaign: PropTypes.object,
    history: PropTypes.object,
    pushState: PropTypes.func.isRequired,
    editCampaign: PropTypes.func,
    saveCampaign: PropTypes.func,
    cancelEditingCampaign: PropTypes.func,
    clearCampaignDetails: PropTypes.func,
    rates: PropTypes.array,
    bars: PropTypes.array,
    onMessageSocket: PropTypes.func,
    campaignId: PropTypes.string,
    updateCampaign: PropTypes.func,
    allTags: PropTypes.array
  }

  componentDidMount() {
    // live graphs
    const {onMessageSocket, campaignId} = this.props;
    webSocket = new WebSocket(`ws://${config.socketHost}:${config.socketPort}/v1/campaigns/${campaignId}`);

    webSocket.onmessage = function(event) {
      console.log('SOCKET_MESSAGE CAMPAIGN');
      const rec = JSON.parse(event.data);
      const rdata = rec.data;
      const rate = rec.rate;
      onMessageSocket({rdata: rdata, rate: rate});
    };
  }

  componentWillUnmount() {
    const {clearCampaignDetails} = this.props;
    clearCampaignDetails();
    webSocket.close();
  }

  handleSubmit = (data) => {
    data.tags = refactorTagsAPI(data.tags);
    this.props.updateCampaign(this.props.campaign.data.id, data)
      .then(res => {if (! _.has(res, 'error') ) window.location.reload(); });
  }

  render() {
    const { campaign, editCampaign, cancelEditingCampaign, allTags, rates, bars, pushState } = this.props;

    const rateChartData = {
      data: {
        x: 'x',
        columns: rates
      },
      axis: {
        x: {
          type: 'timeseries',
          tick: {
            format: '%H:%M:%S'
          }
        }
      },
      bindto: '#rateChartCampaign'
    };

    const barChartData = {
      data: {
        columns: bars,
        type: 'bar'
      },
      bar: {
        width: {
          ratio: 0.5 // this makes bar width 50% of length between ticks
        }
      },
      axis: {
        x: {
          type: 'category',
          categories: ['EMAIL', 'SMS', 'PUSH NOTIFICATIONS']
        }
      },
      bindto: '#barChartCampaign'
    };

    return (
      <div>
        <div className="panel-body">
          <div className="row">
            <div className="col-lg-12">
              <div className="ibox float-e-margins">
                <div className="ibox-title">
                  <h5>Basic info</h5>
                </div>
                <div className="ibox-content p-md">
                  <div>
                    {!campaign.editing &&
                    <form className="form-horizontal">
                      <FormControls.Static label="Name" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={campaign.data.name}/>
                      <FormControls.Static label="Start date" labelClassName="col-xs-2" wrapperClassName="col-xs-10"> <DateLabel date={campaign.data.startDate}/> </FormControls.Static>
                      <FormControls.Static label="End date" labelClassName="col-xs-2" wrapperClassName="col-xs-10"><DateLabel date={campaign.data.endDate}/></FormControls.Static>
                      <TagDisplayForm labelClassName="col-xs-2" wrapperClassName="col-xs-10" tags={campaign.data.tags} />
                      <div className="hr-line-dashed"></div>
                      <div className="row">
                        <div className="col-xs-offset-2 col-xs-10">
                          <ButtonToolbar>
                            <Button onClick={()=> pushState(null, `/messages/new?campaignId=${campaign.data.id}` )}><Glyphicon glyph="envelope" /> &nbsp;Send a message</Button>
                            <Button onClick={editCampaign}><Glyphicon glyph="edit" /> &nbsp;Edit</Button>
                          </ButtonToolbar>
                        </div>
                      </div>
                    </form>
                    }
                    {campaign.editing &&
                    <CampaignForm allTags={refactorTags(allTags)} onSubmit={this.handleSubmit} handleCancel={cancelEditingCampaign}/>
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-lg-12">
              <div className="ibox float-e-margins">
                <div className="ibox-title">
                  <h5>Live rate</h5>
                </div>
                <div className="ibox-content">
                  <div className="row">
                    <div className="col-lg-12">
                      <LineChart data={rateChartData} id="rateChartCampaign" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-12">
              <div className="ibox float-e-margins">
                <div className="ibox-title">
                  <h5>Live distribution</h5>
                </div>
                <div className="ibox-content">
                  <div className="row">
                    <div className="col-lg-12">
                      <BarChart data={barChartData} id="barChartCampaign" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    );
  }
  }
