/**
 * Created by urbanmarovt on 01/03/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as routerActions from 'redux-router';

import { ChannelStatistics, PackagesGrid} from 'components';
import {LineChart} from 'components';
import {ButtonToolbar, ButtonGroup, Button} from 'react-bootstrap';

import {loadCountByChannels, loadHistoryByChannels} from 'redux/modules/models/messages/count';
import {load as loadPackages} from 'redux/modules/models/packages/list';

function fetchDataDeferred(getState, dispatch) {
  const campaignId = getState().router.params.campaignId;

  const promises = [];
  promises.push(dispatch(loadCountByChannels('d', campaignId)));
  promises.push(dispatch(loadHistoryByChannels('d', campaignId)));
  promises.push(dispatch(loadPackages(10, 0, campaignId)));
  return Promise.all(promises);
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    countByChannels: state.models.messages.count.byChannels,
    historyByChannels: state.models.messages.count.history,
    packages: state.models.packages.list.data
  }),
  dispatch => bindActionCreators({...routerActions, loadCountByChannels, loadHistoryByChannels, loadPackages}, dispatch)
)
export default class CampaignStatistics extends Component {

  static propTypes = {
    countByChannels: PropTypes.object,
    historyByChannels: PropTypes.object,
    packages: PropTypes.array,
    loadCountByChannels: PropTypes.func,
    loadHistoryByChannels: PropTypes.func,
    loadPackages: PropTypes.func,
    params: PropTypes.object,
    pushState: PropTypes.func
  };

  onHistoryTabClick = (timePeriod) => {
    const {loadCountByChannels, loadHistoryByChannels, params: {campaignId}} = this.props;
    loadCountByChannels(timePeriod, campaignId);
    loadHistoryByChannels(timePeriod, campaignId);
  }

  onPackageClick = (id)=> {
    const {pushState} = this.props;
    pushState(null, `/packages/${id}`, '');
  }

  render() {

    const {countByChannels, historyByChannels, packages} = this.props;

    const lineChartData2 = {
      data: {
        x: 'x',
        columns: [
          [...historyByChannels.x],
          [...historyByChannels.sms],
          [...historyByChannels.email],
          [...historyByChannels.notification]
        ]
      },
      axis: {
        x: {
          type: 'timeseries'
        },
        y: {
          label: 'messages'
        }
      }
    };

    return (
      <div className="panel-body">
        <div className="row">
          <div className="col-lg-12">
            <div className="ibox float-e-margins">
              <div className="ibox-title">
                <h5>Channels</h5>
                <div className="pull-right">
                  <ButtonToolbar>
                    <ButtonGroup bsSize="xsmall">
                      <Button onClick={() => this.onHistoryTabClick('d')}>Today</Button>
                      <Button onClick={() => this.onHistoryTabClick('M')}>Monthly</Button>
                      <Button onClick={() => this.onHistoryTabClick('y')}>Annual</Button>
                    </ButtonGroup>
                  </ButtonToolbar>
                </div>
              </div>
              <div className="ibox-content">
                <div className="row">
                  <div className="col-lg-3">
                    <h1>
                      {countByChannels.all}
                    </h1>
                    <h5>Total messages in period</h5>
                  </div>
                  <div className="col-lg-9">
                    <ul className="stat-list">
                      <li>
                        <ChannelStatistics label="Total SMS in period" channelCount={countByChannels.sms} channelPercent={countByChannels.smsPercent} />
                      </li>
                      <li>
                        <ChannelStatistics label="Total Email in period" channelCount={countByChannels.email} channelPercent={countByChannels.emailPercent} />
                      </li>
                      <li>
                        <ChannelStatistics label="Total Notifications in period" channelCount={countByChannels.notification} channelPercent={countByChannels.notificationPercent} />
                      </li>
                    </ul>
                  </div>
                </div>
                <br/>
                <br/>
                <div className="row">
                  <LineChart data={lineChartData2} id="lineChartHistory" />
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-lg-12">
            <div className="ibox float-e-margins">
              <div className="ibox-title">
                <h5>Last messages</h5>
                <div className="pull-right">
                  <div className="btn-group">
                    <button type="button" className="btn btn-xs btn-white active">All</button>
                    <button type="button" className="btn btn-xs btn-white">Completed</button>
                    <button type="button" className="btn btn-xs btn-white">Pending</button>
                    <button type="button" className="btn btn-xs btn-white">Undelivered</button>
                  </div>
                </div>
              </div>
              <div className="ibox-content">
                <PackagesGrid
                  packages={packages}
                  onPackageClick={this.onPackageClick}
                />
              </div>
            </div>
          </div>
        </div>

      </div>
    );
  }
}


