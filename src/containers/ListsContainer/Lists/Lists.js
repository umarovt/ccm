/**
 * Created by rokfortuna on 3/10/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';
import {reduxForm} from 'redux-form';

import {getLimitOffsetURL, getOffset, getPagesCount, getActivePage} from 'utils/pagination';
import {getQueryParams, toggleOrder, getSearchTags, setQueryParams} from 'utils/queryParams';
import {
  onPageChange,
  onSearchStringChange,
  onOrderChange,
  setInitialState as setInitialStateQueryParams} from 'redux/modules/queryParams';

import {ListsGrid} from 'components';

const defaultQueryParams = {
  limit: 10,
  offset: 0,
  order: '',
  searchString: '',
  searchTags: ''
}

function fetchDataDeferred(getState, dispatch) {
  const promises = [];

//  let queryParams = getQueryParams(getState().router, defaultQueryParams);

  return Promise.all(promises);
}

const readQueryParams = (router) => {
  const {searchString} = getQueryParams(router, defaultQueryParams);
  const searchTags = getSearchTags(router);

  return {
    searchString: searchString,
    searchTags: searchTags
  }
};

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
  }),
  dispatch => bindActionCreators(
    {...routerActions,
      onPageChange,
      onSearchStringChange,
      onOrderChange,
      setInitialStateQueryParams
    }, dispatch)
)
export default class Lists extends Component {

  static propTypes = {

  }


  onListClick = (id) => {
    const {pushState} = this.props;
    pushState(null, `/lists/${id}`, '');
  };

  render() {
//    const { } = this.props;

//    const {limit, offset} = this.props.queryParams;

    const mockLists = [
      {id: 1, name: 'Golfisti', numReceivers: 4489},
      {id: 2, name: 'Promocije turizma', numReceivers: 11045},
      {id: 3, name: 'MM turizem partnerji', numReceivers: 114}
    ]

    return (
      <div>
        <div className="row wrapper border-bottom white-bg page-heading">
          <div className="col-lg-10">
            <h2>Lists</h2>
          </div>
          <div className="col-lg-2"></div>
        </div>
        <div className="wrapper wrapper-content animated fadeInRight p-b-none">
          <div className="row">
            <div className="col-lg-12">
              <div className="ibox float-e-margins">
                <div className="ibox-content p-md">
                  <ListsGrid
                    lists={mockLists}
                    onListClick={this.onListClick}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}


//  onPageChangeClick = (error, selectedPage) => {
//    const { loadCampaigns, queryParams, onPageChange } = this.props;
//    const newQueryParams = {...queryParams, offset: getOffset(selectedPage.eventKey, queryParams.limit) };
//    onPageChange("/campaigns", newQueryParams);
//    loadCampaigns(newQueryParams);
//  };

//  onFormStateChange = (field, fieldValue) => {
//
//    const {  loadCampaigns, queryParams, onSearchStringChange  } = this.props;
//
//    let newQueryParams = {...queryParams};
//    if (field.name === 'searchTags') {
//      newQueryParams = {
//        ...newQueryParams,
//        searchTags: fieldValue,
//        offset: 0
//      };
//      loadCampaigns(newQueryParams);
//
//    } else if (field.name === 'searchString') {
//      newQueryParams = {
//        ...newQueryParams,
//        searchString: fieldValue,
//        offset: 0
//      };
//      loadCampaigns(newQueryParams);
//    }
//    onSearchStringChange("/campaigns", newQueryParams);
//
//  }

//  makeOnChangeInput = (field) => {
//    return (event) => {
//      field.onChange(event);
//      this.onFormStateChange(field, event.target.value);
//    };
//  };
//
//  makeOnChangeMultiSelect = (field) => {
//    return (value) => {
//      field.onChange(value);
//      this.onFormStateChange(field, value);
//    };
//  };

//  onSortClick = (field)=> {
//    const { loadCampaigns, queryParams, onOrderChange } = this.props;
//    const newQueryParams = {
//      ...queryParams,
//      order: toggleOrder(field, queryParams.order),
//      limit: defaultQueryParams.limit,
//      offset:0
//    };
//    onOrderChange("/campaigns", newQueryParams);
//    loadCampaigns(newQueryParams);
//  }
