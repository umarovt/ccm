import React, { Component, PropTypes } from 'react';
import DocumentMeta from 'react-document-meta';
import config from '../../config';

import {SideMenu, TopMenu} from 'components';

export default class App extends Component {
  static propTypes = {
    children: PropTypes.object.isRequired,
  };

  static contextTypes = {
    store: PropTypes.object.isRequired
  };

  render() {
    const styles = require('./App.scss');

    return (
      <div className={styles.app}>
        <DocumentMeta {...config.app}/>
        <div id="wrapper">
          <SideMenu />
          <div id="page-wrapper" className="gray-bg">

            <TopMenu />

            <div className={`${styles.appContent}`}>
              {this.props.children}
            </div>

            <div className="footer">
              <div>
                <strong>Copyright</strong> KumuluzEE &copy; 2014-2016
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
