/**
 * Created by rokfortuna on 25/02/16.
 */

import { combineReducers } from 'redux';
import dashboardReducer from './dashboard';
import campaignReducer from './campaign';

export default combineReducers({
  dashboard: dashboardReducer,
  campaign: campaignReducer
});
