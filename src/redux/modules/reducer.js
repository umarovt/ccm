import { combineReducers } from 'redux';
import { routerStateReducer } from 'redux-router';

import {reducer as form } from 'redux-form';
import modelsReducer from './models/reducer';
import viewsReducer from './views/reducer';
import queryParams from './queryParams';

export default combineReducers({
  router: routerStateReducer,
  form,
  models: modelsReducer,
  views: viewsReducer,
  queryParams: queryParams
});
