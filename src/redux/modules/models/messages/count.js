/**
 * Created by rokfortuna on 26/02/16.
 */

import moment from 'moment';
import {postgresDate, postgresDateTime} from '../../../../utils/datetimeFormat';

const COUNT_MESSAGES_BY_TIME = 'kumuluzccm/messages/COUNT_BY_TIME';
const COUNT_MESSAGES_BY_TIME_SUCCESS = 'kumuluzccm/messages/COUNT_BY_TIME_SUCCESS';
const COUNT_MESSAGES_BY_TIME_FAIL = 'kumuluzccm/messages/COUNT_BY_TIME_FAIL';
const COUNT_MESSAGES_BY_CHANNELS = 'kumuluzccm/messages/COUNT_BY_CHANNELS';
const COUNT_MESSAGES_BY_CHANNELS_SUCCESS = 'kumuluzccm/messages/COUNT_BY_CHANNELS_SUCCESS';
const COUNT_MESSAGES_BY_CHANNELS_FAIL = 'kumuluzccm/messages/COUNT_BY_CHANNELS_FAIL';
const HISTORY_MESSAGES_BY_CHANNELS = 'kumuluzccm/messages/HISTORY_BY_CHANNELS';
const HISTORY_MESSAGES_BY_CHANNELS_SUCCESS = 'kumuluzccm/messages/HISTORY_BY_CHANNELS_SUCCESS';
const HISTORY_MESSAGES_BY_CHANNELS_FAIL = 'kumuluzccm/messages/HISTORY_BY_CHANNELS_FAIL';


const initialState = {
  byTime: {
    allTime: {count: 1, delivered: 1, opened: 1},
    annual: {count: 1, delivered: 1, opened: 1},
    monthly: {count: 1, delivered: 1, opened: 1},
    today: {count: 1, delivered: 1, opened: 1},
  },
  byChannels: {
    all: 1,
    sms: 1,
    smsPercent: 1,
    email: 1,
    emailPercent: 1,
    notification: 1,
    notificationPercent: 1
  },
  history: {
    x: ['x'],
    sms: ['SMS'],
    email: ['EMAIL'],
    notification: ['PUSH_NOTIFICATION']
  }
};

const refactorXAxisData = (apiData, label) => {
  const data = [label];
  return apiData.reduce((prev, curr) => {return [...prev, curr.date];}, data);
};

const refactorYAxisData = (apiData, label) => {
  const data = [label];
  return apiData.reduce((prev, curr) => {return [...prev, curr.value];}, data);
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case COUNT_MESSAGES_BY_TIME:
      return {
        ...state
      };
    case COUNT_MESSAGES_BY_TIME_SUCCESS:
      return {
        ...state,
        byTime: {
          allTime: {
            count: action.result[0].body,
            delivered: action.result[1].body,
            opened: action.result[2].body },
          annual: {
            count: action.result[3].body,
            delivered: action.result[4].body,
            opened: action.result[5].body },
          monthly: {
            count: action.result[6].body,
            delivered: action.result[7].body,
            opened: action.result[8].body},
          today: {
            count: action.result[9].body,
            delivered: action.result[10].body,
            opened: action.result[11].body }
        },

      };
    case COUNT_MESSAGES_BY_TIME_FAIL:
      return {
        ...state
      };
    case COUNT_MESSAGES_BY_CHANNELS:
      return {
        ...state
      };
    case COUNT_MESSAGES_BY_CHANNELS_SUCCESS:
      return {
        ...state,
        byChannels: {
          all: action.result[0].body,
          sms: action.result[1].body,
          smsPercent: action.result[1].body / action.result[0].body * 100,
          email: action.result[2].body,
          emailPercent: action.result[2].body / action.result[0].body * 100,
          notification: action.result[3].body,
          notificationPercent: action.result[3].body / action.result[0].body * 100
        }
      };
    case COUNT_MESSAGES_BY_CHANNELS_FAIL:
      return {
        ...state
      };
    case HISTORY_MESSAGES_BY_CHANNELS:
      return {
        ...state
      };
    case HISTORY_MESSAGES_BY_CHANNELS_SUCCESS:
      return {
        ...state,
        history: {
          x: refactorXAxisData(action.result[0].body, 'x'),
          sms: refactorYAxisData(action.result[0].body, 'SMS'),
          email: refactorYAxisData(action.result[1].body, 'EMAIL'),
          notification: refactorYAxisData(action.result[2].body, 'PUSH_NOTIFICATION')
        }
      };
    case HISTORY_MESSAGES_BY_CHANNELS_FAIL:
      return {
        ...state
      };
    default:
      return state;
  }
}

export function loadCountByTime() {

  return {
    types: [COUNT_MESSAGES_BY_TIME, COUNT_MESSAGES_BY_TIME_SUCCESS, COUNT_MESSAGES_BY_TIME_FAIL],
    promise:

      (client) => Promise.all(
        [
          // All time
          client.get('/v1/messages/count'),
          client.get('/v1/messages/count', { params: {where: 'deliveryStatus:EQ:DELIVERED'}}),
          client.get('/v1/messages/count', { params: {where: 'deliveryStatus:EQ:CONFIRMED_BY_USER'}}),
          // Annual
          client.get('/v1/messages/count', {params: {where: `createdAt:GTE:${postgresDate(moment().subtract(1, 'y'))}`}}),
          client.get('/v1/messages/count', { params: {where: `createdAt:GTE:${postgresDate(moment().subtract(1, 'y'))} deliveryStatus:EQ:DELIVERED` }}),
          client.get('/v1/messages/count', { params: {where: `createdAt:GTE:${postgresDate(moment().subtract(1, 'y'))}  deliveryStatus:EQ:CONFIRMED_BY_USER`}}),
          // Monthly
          client.get('/v1/messages/count', {params: {where: `createdAt:GTE:${postgresDate(moment().subtract(1, 'M'))}`}}),
          client.get('/v1/messages/count', { params: {where: `createdAt:GTE:${postgresDate(moment().subtract(1, 'M'))} deliveryStatus:EQ:DELIVERED` }}),
          client.get('/v1/messages/count', { params: {where: `createdAt:GTE:${postgresDate(moment().subtract(1, 'M'))} deliveryStatus:EQ:CONFIRMED_BY_USER`}}),
          // Today
          client.get('/v1/messages/count', {params: {where: `createdAt:GT:${postgresDate(moment().subtract(1, 'd'))}`}}),
          client.get('/v1/messages/count', { params: {where: `createdAt:GT:${postgresDate(moment().subtract(1, 'd'))} deliveryStatus:EQ:DELIVERED` }}),
          client.get('/v1/messages/count', { params: {where: `createdAt:GT:${postgresDate(moment().subtract(1, 'd'))} deliveryStatus:EQ:CONFIRMED_BY_USER`}})
        ])
  };
}

export function loadCountByChannels(timePeriod, campaignId = '') {

  return {
    types: [COUNT_MESSAGES_BY_CHANNELS, COUNT_MESSAGES_BY_CHANNELS_SUCCESS, COUNT_MESSAGES_BY_CHANNELS_FAIL],
    promise:
      (client) => Promise.all(
        [
          // all
          client.get('/v1/messages/count', {params: {where: `createdAt:GT:${postgresDate(moment().subtract(1, timePeriod))} messagePackage.campaign.id:EQ:${campaignId}`}}),
          // sms
          client.get('/v1/messages/count', {params: {where: `createdAt:GT:${postgresDate(moment().subtract(1, timePeriod))} channelType:EQ:SMS messagePackage.campaign.id:EQ:${campaignId}`}}),
          // email
          client.get('/v1/messages/count', {params: {where: `createdAt:GT:${postgresDate(moment().subtract(1, timePeriod))} channelType:EQ:EMAIL messagePackage.campaign.id:EQ:${campaignId}`}}),
          // notification
          client.get('/v1/messages/count', {params: {where: `createdAt:GT:${postgresDate(moment().subtract(1, timePeriod))} channelType:EQ:PUSH_NOTIFICATION messagePackage.campaign.id:EQ:${campaignId}`}})
        ]
      )
  };
}

export function loadHistoryByChannels(timePeriod, campaignId = '') {
  let groupBy = 'month';
  if (timePeriod === 'd') {
    groupBy = 'hour';
  } else if (timePeriod === 'M') {
    groupBy = 'day';
  }

  return {
    types: [HISTORY_MESSAGES_BY_CHANNELS, HISTORY_MESSAGES_BY_CHANNELS_SUCCESS, HISTORY_MESSAGES_BY_CHANNELS_FAIL],
    promise:
      (client) => Promise.all(
        [
          // sms
          client.get('/v1/messages/history', {params: {
            from: `${postgresDateTime(moment().subtract(1, timePeriod))}`,
            to: `${postgresDateTime(moment())}`,
            groupBy: groupBy,
            channelType: `SMS`,
            campaignId: campaignId
          }}),
          // email
          client.get('/v1/messages/history', {params: {
            from: `${postgresDateTime(moment().subtract(1, timePeriod))}`,
            to: `${postgresDateTime(moment())}`,
            groupBy: groupBy,
            channelType: `EMAIL`,
            campaignId: campaignId
          }}),
          // notification
          client.get('/v1/messages/history', {params: {
            from: `${postgresDateTime(moment().subtract(1, timePeriod))}`,
            to: `${postgresDateTime(moment())}`,
            groupBy: groupBy,
            channelType: `PUSH_NOTIFICATION`,
            campaignId: campaignId
          }})
        ]
      )
  };
}
