/**
 * Created by urbanmarovt on 03/03/16.
 */

const LOAD = 'kumuluzccm/receiver/LOAD';
const LOAD_SUCCESS = 'kumuluzccm/receiver/LOAD_SUCCESS';
const LOAD_FAIL = 'kumuluzccm/receiver/LOAD_FAIL';
const CLEAR_STATE = 'kumuluzccm/receiver/CLEAR_STATE';

const initialState = {
  loaded: false,
  error: {},
  data: {},
};

export default function reducer(state = initialState, action = {}) {

  switch (action.type) {
    case LOAD:
      return {
        ...state,
        data: {},
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.body,
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: {},
        error: action.error
      };
    case CLEAR_STATE:
      return initialState;
    default:
      return state;
  }
}

export function load(globalState) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client) => client.get(`/v1/messages/${globalState.router.params.messageId}`, {})
  };
}

export function clearState() {
  return {
    type: CLEAR_STATE
  };
}
