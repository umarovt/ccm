/**
 * Created by urbanmarovt on 09/02/16.
 */

const LOAD = 'kumuluzccm/packages/LOAD';
const LOAD_SUCCESS = 'kumuluzccm/packages/LOAD_SUCCESS';
const LOAD_FAIL = 'kumuluzccm/packages/LOAD_FAIL';

const initialState = {
  loaded: false,
  error: {},
  data: [],
  count: 0
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        data: [],
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.body,
        count: parseInt(action.result.headers['x-total-count']),
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: [],
        count: 0,
        error: action.error
      };
    default:
      return state;
  }
}

// put globalState as parameters
export function load(limit, offset, campaignId = '') {

  const params = {
    limit: limit,
    offset: offset,
    campaignId: campaignId
  };

  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client) => client.get('/v1/messagepackages/list', { params: params})
  };
}
