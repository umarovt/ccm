/**
 * Created by rokfortuna on 03/03/16.
 */

const LOAD = 'kumuluzccm/package/LOAD';
const LOAD_SUCCESS = 'kumuluzccm/package/LOAD_SUCCESS';
const LOAD_FAIL = 'kumuluzccm/package/LOAD_FAIL';
const LOAD_MESSAGES = 'kumuluzccm/package/LOAD_MESSAGES';
const LOAD_MESSAGES_SUCCESS = 'kumuluzccm/package/LOAD_MESSAGES_SUCCESS';
const LOAD_MESSAGES_FAIL = 'kumuluzccm/package/LOAD_MESSAGES_FAIL';

const initialState = {
  loaded: false,
  error: {},
  package: {
    totalCount: 0,
  },
  messages: {
    data: [],
    totalCount: 0
  }
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        package: {
          totalCount: 0
        },
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        package: {
          ...action.result[0].body,
          totalCount: action.result[1].body
        },
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        package: {
          totalCount: 0
        },
        error: action.error
      };
    case LOAD_MESSAGES:
      return {
        ...state
      };
    case LOAD_MESSAGES_SUCCESS:
      return {
        ...state,
        messages: {
          data: action.result.body,
          totalCount: action.result.headers['x-total-count']
        }
      };
    case LOAD_MESSAGES_FAIL:
      return {
        ...state,
        messages: {data: [], totalCount: 0},
        error: action.error
      };
    default:
      return state;
  }
}


export function load(packageId) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client) => Promise.all(
      [client.get(`/v1/messagepackages/${packageId}`),
        client.get(`/v1/messages/count`, {params: {where: `messagePackage.id:EQ:${packageId}`}}) ])
  };
}

export function loadMessages(packageId, queryParams) {

  const {limit, offset, searchString, order} = queryParams;

  const params = {
    limit: limit,
    offset: offset,
    where: `messagePackage.id:EQ:${packageId}`,
    order: order
  };

  if (searchString !== undefined && searchString !== 'undefined' && searchString !== '') {
    params.where = `${params.where} receiver.email:likeic:%${searchString}%`;
  }

  return {
    types: [LOAD_MESSAGES, LOAD_MESSAGES_SUCCESS, LOAD_MESSAGES_FAIL],
    promise: (client) =>
        client.get(`/v1/messages/`,
          {params: params})
  };
}

