/**
 * Created by urbanmarovt on 15/02/16.
 */

const CREATE = 'around/packages/CREATE';
const CREATE_SUCCESS = 'around/packages/CREATE_SUCCESS';
const CREATE_FAIL = 'around/packages/CREATE_FAIL';

const initialState = {
  creating: false,
  created: false,
  error: {}
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case CREATE:
      return {
        ...state,
        creating: true,
        created: false
      };
    case CREATE_SUCCESS:
      return {
        ...state,
        creating: false,
        created: true,
        error: {}
      };
    case CREATE_FAIL:
      return {
        ...state,
        creating: false,
        created: false,
        error: action.error
      };
    default:
      return state;
  }
}

// put globalState as parameters
export function create(data) {
  return {
    types: [CREATE, CREATE_SUCCESS, CREATE_FAIL],
    promise: (client) => client.post('/v1/messagepackages', {
      data: {
        campaign: {
          id: data.campaign
        },
        referent: 'Ziga Novak' // TODO get account information

      }
    })
  };
}
