import { combineReducers } from 'redux';

import packageNew from './new';
import packagesList from './list';
import packageDetails from './details';

export default combineReducers({
  new: packageNew,
  list: packagesList,
  details: packageDetails
});
