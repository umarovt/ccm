import { combineReducers } from 'redux';

import tags from './tags';
import campaingsReducer from './campaigns/reducer';
import messagesReducer from './messages/reducer';
import receiversReducer from './receivers/reducer';
import packagesReducer from './packages/reducer';
export default combineReducers({
  campaigns: campaingsReducer,
  tags: tags,
  receivers: receiversReducer,
  messages: messagesReducer,
  packages: packagesReducer
});
