import { combineReducers } from 'redux';

import prefsNew from './new';
import prefsList from './list';

export default combineReducers({
  new: prefsNew,
  list: prefsList
});
