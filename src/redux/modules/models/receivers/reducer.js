import { combineReducers } from 'redux';

import receivers from './list';
import details from './details';
import preferences from './preferences/reducer';

export default combineReducers({
  list: receivers,
  details: details,
  preferences: preferences
});
