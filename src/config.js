require('babel/polyfill');

const environment = {
  development: {
    isProduction: false
  },
  production: {
    isProduction: true
  }
}[process.env.NODE_ENV || 'development'];

module.exports = Object.assign({
  host: process.env.HOST || 'localhost',
  port: process.env.PORT,
  apiHost: process.env.APIHOST || 'localhost',
  apiPort: process.env.APIPORT || '8080',
  socketHost: process.env.SOCKETHOST || 'localhost',
  socketPort: process.env.SOCKETPORT || '9999',
  app: {
    title: 'KumuluzCCM',
    description: 'KumuluzCCM app.',
    meta: {
      charSet: 'utf-8',
      property: {
        'og:site_name': 'KumuluzCCM',
        'og:image': 'https://react-redux.herokuapp.com/logo.jpg',
        'og:locale': 'en_US',
        'og:title': 'KumuluzCCM',
        'og:description': 'KumuluzCCM description.',
      }
    }
  }
}, environment);
