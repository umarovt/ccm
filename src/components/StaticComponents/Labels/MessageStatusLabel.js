/**
 * Created by rokfortuna on 3/7/16.
 */

import React, {Component, PropTypes} from 'react';

export default class MessageStatusLabel extends Component {

  static propTypes = {
    type: PropTypes.string
  }

  types = {
    OPENED: {className: 'label-primary', label: 'opened'},
    DELIVERED: {className: 'label-warning', label: 'delivered'}
  }

  render() {
    const {type} = this.props;
    const types = this.types;
    return (
      <div>
        { types[type] !== undefined &&
          <span className={`label ${types[type].className}`}>{types[type].label}</span>
        }
        { types[type] === undefined &&
          <span> {type} </span>
        }
      </div>
    );
  }
}
