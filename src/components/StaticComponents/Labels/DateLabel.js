  /**
 * Created by rokfortuna on 12/02/16.
 */

import React, {Component, PropTypes} from 'react';
import {dateFromTimestamp} from 'utils/datetimeFormat';

export default class DateLabel extends Component {

  static propTypes = {
    date: PropTypes.number
  }

  render() {
    const {date} = this.props;
    return (
        <span>
          <i className="fa fa-icon fa-calendar"/>
          &nbsp; &nbsp; &nbsp;
          {date && dateFromTimestamp(date)}
          {!date && '-'}
        </span>
    );
  }
}
