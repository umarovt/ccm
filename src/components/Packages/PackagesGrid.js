import React, {Component, PropTypes} from 'react';
import {Table} from 'react-bootstrap';
import {DateLabel} from 'components';

export default class PackagesGrid extends Component {
  static propTypes = {
    packages: PropTypes.array,
    onPackageClick: PropTypes.func,
    paginationOnPageChange: PropTypes.func,
    paginationPages: PropTypes.number,
    paginationActivePage: PropTypes.number
  };

  render() {
    const {packages, onPackageClick, paginationOnPageChange, paginationPages, paginationActivePage} = this.props;
    return (
      <div>
        <Table className="table" hover>
          <thead>
            <tr>
              <th>
                <strong>Date</strong>
              </th>
              <th>
                <strong>Referent</strong>
              </th>
              <th>
                <strong>Campaign</strong>
              </th>
              <th>
                <strong>Number of messages</strong>
              </th>
            </tr>
          </thead>
          <tbody>
          { packages.map(
            (pcg) =>
              <tr key={pcg.id} onClick={()=> onPackageClick(pcg.id)} className="clickable">
                <td>
                  <DateLabel date={pcg.createdAt}/>
                </td>
                <td>{pcg.referent}</td>
                <td>{pcg.campaign}</td>
                <td>{pcg.numOfMessages}</td>
              </tr>
            )
          }
          </tbody>
        </Table>
      </div>
    );
  }
}
