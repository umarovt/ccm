/**
 * Created by urbanmarovt on 19/02/16.
 */

import React, {Component, PropTypes} from 'react';
import {Table, Tr, Td, Thead, Th} from 'reactable';

export default class ReceiversPrefsGrid extends Component {
  static propTypes = {
    receiversPrefs: PropTypes.array.isRequired
  };

  render() {
    const {receiversPrefs} = this.props;

    return (
      <div>
        <Table className="table" sortable>
          <Thead>
          <Th column="Channel type">
            <strong className="name-header">Channel type</strong>
          </Th>
          <Th column="Receive from">
            <strong className="name-header">Receive from</strong>
          </Th>
          <Th column="Receive to">
            <strong className="name-header">Receive to</strong>
          </Th>
          <Th column="Receive days">
            <strong className="name-header">Receive days</strong>
          </Th>
          <Th column="Enabled">
            <strong className="name-header">Enabled</strong>
          </Th>
          <Th column="Priority">
            <strong className="name-header">Priority</strong>
          </Th>
          </Thead>
          {
            receiversPrefs.map(
              (prefs) =>
                <Tr key={prefs.id}>
                  <Td column="Channel type">
                    {prefs.channelType}
                  </Td>
                  <Td column="Receive from">
                    {prefs.receiveFrom}
                  </Td>
                  <Td column="Receive to">
                    {prefs.receiveTo}
                  </Td>
                  <Td column="Receive days">
                    {prefs.receiveDays}
                  </Td>
                  <Td column="Enabled">
                    {!prefs.disabled}
                  </Td>
                  <Td column="Priority">
                    {prefs.priority}
                  </Td>
                </Tr>
            )
          }
        </Table>
      </div>
    );
  }
}
