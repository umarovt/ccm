/**
 * Created by urbanmarovt on 01/03/16.
 */
import React, {Component, PropTypes} from 'react';
import {reduxForm} from 'redux-form';
import {Input, Button, ButtonToolbar, Glyphicon} from 'react-bootstrap';

import { validateEmail, validateMobilePhone } from 'utils/validation';
import _ from 'underscore';


const validate = (values) => {

  const errors = {};
  if (!values.firstName) {
    errors.firstName = 'Required';
  }

  if (!values.lastName) {
    errors.lastName = 'Required';
  }

  if (!values.email) {
    errors.email = 'Required';
  } else if (validateEmail(values.email)) {
    errors.email = 'Email format is not correct';
  }

  if (!values.mobilePhone) {
    errors.mobilePhone = 'Required';
  } else if (!validateMobilePhone(values.mobilePhone)) {
    errors.mobilePhone = 'Mobile phone format is not correct';
  }

  return errors;
};

function reformatPrefillData(initialValues) {
  if (_.isEmpty(initialValues)) return {};
  const formattedData = {
    ...initialValues
  };
  return formattedData;
}

@reduxForm({
  form: 'ReceiverForm',
  fields: ['firstName', 'lastName', 'email', 'mobilePhone'],
  validate: validate
},
state => ({
  initialValues: reformatPrefillData(state.models.receivers.details.data)
}))
export default class ReceiverForm extends Component {

  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    handleCancel: PropTypes.func.isRequired,
    errors: PropTypes.object,
    submitting: PropTypes.bool,
    fields: PropTypes.object
  }

  render() {
    const {fields: {firstName, lastName, email, mobilePhone}, handleSubmit, handleCancel, submitting, errors} = this.props;

    // validation
    const firstNameValidation = {};
    const lastNameValidation = {};
    const emailValidation = {};
    const mobilePhoneValidation = {};

    if (firstName.touched && firstName.error) {
      firstNameValidation.help = firstName.error;
      firstNameValidation.bsStyle = 'error';
    }

    if (lastName.touched && lastName.error) {
      lastNameValidation.help = lastName.error;
      lastNameValidation.bsStyle = 'error';
    }
    if (email.touched && email.error) {
      emailValidation.help = email.error;
      emailValidation.bsStyle = 'error';
    }
    if (mobilePhone.touched && mobilePhone.error) {
      mobilePhoneValidation.help = mobilePhone.error;
      mobilePhoneValidation.bsStyle = 'error';
    }

    return (
      <form onSubmit={handleSubmit} className="form-horizontal">
        <Input type="text" label="First name" labelClassName="col-xs-2"
               wrapperClassName="col-xs-10" {...firstName} {...firstNameValidation} />
        <Input type="text" label="Last name" labelClassName="col-xs-2"
               wrapperClassName="col-xs-10" {...lastName} {...lastNameValidation} />
        <Input type="text" label="Email" labelClassName="col-xs-2"
               wrapperClassName="col-xs-10" {...email} {...emailValidation} />
        <Input type="text" label="Mobile phone" labelClassName="col-xs-2"
               wrapperClassName="col-xs-10" {...mobilePhone} {...mobilePhoneValidation} />
        <div className="hr-line-dashed"></div>
        <div className="row">
          <div className="col-xs-offset-2 col-xs-10">
            <ButtonToolbar>
              <Button type="submit" disabled={submitting || Object.keys(errors).length !== 0}>
                <Glyphicon glyph="save" />&nbsp;Save
              </Button>
              <Button type="button" disabled={submitting} onClick={handleCancel}><Glyphicon glyph="remove-circle" />&nbsp;Cancel</Button>
            </ButtonToolbar>
          </div>
        </div>
      </form>
    );
  }
}
