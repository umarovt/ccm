/**
 * Created by urbanmarovt on 07/03/16.
 */

import React, {Component, PropTypes} from 'react';

import {ReceiverPref} from 'components';

import Sortable from 'react-anything-sortable';


export default class ReceiversPrefs extends Component {
//  mixins = [SortableMixin];

  static propTypes = {
    receiverPrefs: PropTypes.array.isRequired,
    onCreateClick: PropTypes.func,
    receiverId: PropTypes.string
  };

  handleSort = (sortData) => {
    console.log('HANDLE SORT');
    console.log(sortData);
  }

  render() {
    const {receiverPrefs, receiverId, onCreateClick} = this.props;

    return (
      <Sortable onSort={this.handleSort}>
        {receiverPrefs.map(
          (preference, index) =>
            <ReceiverPref sortData={preference} receiverPref={preference} sort={this.sort} key={index}
                          receiverId={receiverId} onCreateClick={onCreateClick}/>
        )}
      </Sortable>
    );
  }
}
