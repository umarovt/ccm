/**
 * Created by urbanmarovt on 07/03/16.
 */

import React, {Component, PropTypes} from 'react';
import {FormControls} from 'react-bootstrap';

import {TimeLabel} from 'components';

import { sortable } from 'react-anything-sortable';

import styles from './ReceiverPrefs.scss';

import {Button, ButtonToolbar, Glyphicon} from 'react-bootstrap';

@sortable
export default class ReceiverPref extends Component {

  static propTypes = {
    receiverPref: PropTypes.object,
    receiverId: PropTypes.string,
    onCreateClick: PropTypes.func,
    className: PropTypes.string
  };

  render() {
    const {receiverPref, receiverId, onCreateClick} = this.props;

    return (
      <div {...this.props} className={`${this.props.className } ${styles.draggable}`}>
        <div className={styles.draggable}>
          <div className="ibox float-e-margins">
            {receiverPref &&
            <div className="ibox-title">
              <h5>{receiverPref.channelType}</h5>
            </div>}
            <div className="ibox-content">
              {!receiverPref.notDefined &&
                <div>
                  <FormControls.Static label="Receiver after" labelClassName="col-xs-2" wrapperClassName="col-xs-10"><TimeLabel time={receiverPref.receiveFrom} /> </FormControls.Static>
                  <FormControls.Static label="Receiver before" labelClassName="col-xs-2" wrapperClassName="col-xs-10"><TimeLabel time={receiverPref.receiveTo} /> </FormControls.Static>
                  <FormControls.Static label="Receiver days" labelClassName="col-xs-2" wrapperClassName="col-xs-10">{receiverPref.receiveDays}</FormControls.Static>
                  <FormControls.Static label="Channel enabled" labelClassName="col-xs-2" wrapperClassName="col-xs-10">
                    {receiverPref.disabled && <i className="fa fa-remove"/>}
                    {!receiverPref.disabled && <i className="fa fa-check"/>}
                  </FormControls.Static>
                </div>
              }
              {receiverPref.notDefined &&
                <div>
                  <label>Receiver preferences are not defined for this channel.</label>
                  <div className="row">
                    <div className="col-xs-offset-2 col-xs-10">
                      <ButtonToolbar>
                        <Button type="submit" onClick={() => onCreateClick(receiverId, receiverPref.channelType)}>
                          <Glyphicon glyph="save" />&nbsp;Save
                        </Button>
                      </ButtonToolbar>
                    </div>
                  </div>
                </div>
              }
            </div>
          </div>
        </div>
      </div>
    );
  }
}
