/**
 * Created by rokfortuna on 01/03/16.
 */

import React, {Component, PropTypes} from 'react';

let barChart;
export default class BarChart extends Component {

  static propTypes = {
    id: PropTypes.string,
    data: PropTypes.object
  }

  shouldComponentUpdate (nextProps, nextState){
    this.updateChart(nextProps.data.data.columns);
    return false;
  }

  componentDidMount() {
    console.log('component did mount');
    const {data, id} = this.props;
    this.renderChart(data, id);
  }

  updateChart = (newColumns) => {
    barChart.load({
      columns: newColumns
    });
  }

  renderChart = (data, id) => {
    import c3 from './../../../node_modules/c3/c3';

    barChart = c3.generate({
      bindto: `#${id}`,
      ...data
    })
  }


  render() {
    const {data, id} = this.props;
    if(__CLIENT__) {
      this.renderChart(data, id);
    }

    return (
      <div style={{height:'350px'}}>
        <div id={id}></div>
      </div>
    );

  }
};
