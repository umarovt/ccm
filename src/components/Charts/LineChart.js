import React, {Component, PropTypes} from 'react';
import ReactDOM from 'react-dom';


let lineChart;
export default class LineChart extends Component {

  static propTypes = {
    id: PropTypes.string,
    subchart: PropTypes.bool,
    data: PropTypes.object
  }

  shouldComponentUpdate (nextProps, nextState){
    this.updateChart(nextProps.data.data.columns);
    return false;
  }

  componentDidMount() {
    console.log('component did mount');
    const {data, id} = this.props;
    this.renderChart(data, id);
  }

  updateChart = (newColumns) => {
    lineChart.load({
      columns: newColumns
    });
  }

  renderChart = (data, id) => {
    import c3 from './../../../node_modules/c3/c3';
    const { subchart } = this.props;
    lineChart = c3.generate({
      bindto: `#${id}`,
      subchart: {
        show: subchart || false
      },
      ...data
    })
  }


  render() {
    const { id, data} = this.props;



    if(__CLIENT__) {
        this.renderChart(data, id);
    }

    return (
      <div style={{height:'350px'}}>
         <div id={id}></div>
      </div>
    );

  }
};
