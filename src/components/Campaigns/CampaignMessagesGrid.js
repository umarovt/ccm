/**
 * Created by rokfortuna on 23/02/16.
 */
import React, {Component, PropTypes} from 'react';
import {Table, Tr, Td, Thead, Th} from 'reactable';
import {DateLabel} from 'components';
import {Pagination} from 'react-bootstrap';

export default class CampaignsGrid extends Component {
  static propTypes = {
    messages: PropTypes.array.isRequired,
    onMessageClick: PropTypes.func.isRequired,
    paginationOnPageChange: PropTypes.func.isRequired,
    paginationPages: PropTypes.number,
    paginationActivePage: PropTypes.number
  };

  render() {
    const {messages, onMessageClick, paginationOnPageChange, paginationPages, paginationActivePage} = this.props;
    return (
      <div>
        <Table className="table" sortable>
          <Thead>
          <Th column="Status">
            <strong className="name-header">Status &nbsp;<i className="fa fa-sort"></i></strong>
          </Th>
          <Th column="Date">
            <strong className="name-header">Date</strong>
          </Th>
          <Th column="Referent">
            <strong className="name-header">Referent</strong>
          </Th>
          <Th column="Subject">
            <strong className="name-header">Subject</strong>
          </Th>
          <Th column="Delivered">
            <strong className="name-header">Delivered</strong>
          </Th>
          </Thead>
          {
            messages.map(
              (message) =>
                <Tr onClick={() => onMessageClick(message.id)} key={message.id}>
                  <Td column="Status">
                    {message.status}
                  </Td>
                  <Td column="Date">
                    <i className="fa fa-clock-o"></i>{message.date}
                  </Td>
                  <Td column="Referent">
                    {message.referent}
                  </Td>
                  <Td column="Subject">
                    {message.subject}
                  </Td>
                  <Td column="Delivered">
                    {message.delivered}
                  </Td>
                </Tr>
            )
          }
        </Table>
        <Pagination
          prev
          next
          first
          last
          ellipsis
          boundaryLinks
          items={paginationPages}
          maxButtons={5}
          activePage={paginationActivePage}
          onSelect={paginationOnPageChange} />
      </div>
    );
  }
}
