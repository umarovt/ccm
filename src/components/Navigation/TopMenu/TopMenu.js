/**
 * Created by urbanmarovt on 22/02/16.
 */

import React, {Component} from 'react';
import { NavDropdown, MenuItem } from 'react-bootstrap';

export default class TopMenu extends Component {

  render() {
    return (
      <div className="row border-bottom">
        <nav className="navbar navbar-static-top white-bg" role="navigation" style={{marginBottom: 0}}>
          <div className="navbar-header">
            <a className="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i className="fa fa-bars"></i> </a>
            <form role="search" className="navbar-form-custom" action="search_results.html">
              <div className="form-group">
                <input type="text" placeholder="Search" className="form-control" name="top-search" id="top-search"/>
              </div>
            </form>
          </div>
          <ul className="nav navbar-top-links navbar-right">
            <NavDropdown eventKey={3}
                         title={<span className="count-info"><i className="fa fa-lg fa-envelope"><span className="label label-warning">2</span></i></span>}>
              <MenuItem eventKey={3.1} style={{ width: 200 }}>
                <div className="dropdown-messages-box">
                  <div className="media-body">
                    <small className="pull-right">46h ago</small>
                    <strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br/>
                    <small className="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
                  </div>
                </div>
              </MenuItem>
              <MenuItem eventKey={3.1} style={{ width: 200 }}>
                <div className="dropdown-messages-box">
                  <div className="media-body">
                    <small className="pull-right">46h ago</small>
                    <strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br/>
                    <small className="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
                  </div>
                </div>
              </MenuItem>
              <MenuItem divider style={{ width: 200 }} />
              <MenuItem eventKey={3.3} style={{ width: 200 }}><i className="fa fa-lg fa-ellipsis-h"/>&nbsp;Read all</MenuItem>
            </NavDropdown>
            <NavDropdown eventKey={3} title="Tilen Faganel">
              <MenuItem eventKey={3.1}><i className="fa fa-lg fa-user"/>&nbsp;Profile</MenuItem>
              <MenuItem divider />
              <MenuItem eventKey={3.3}><i className="fa fa-lg fa-sign-out"/>&nbsp;Logout</MenuItem>
            </NavDropdown>
          </ul>

        </nav>
      </div>
    );
  }
}

