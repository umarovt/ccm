/**
 * Created by urbanmarovt on 22/02/16.
 */

import React, {Component} from 'react';

import {Link} from 'react-router';

export default class SideMenu extends Component {

  render() {
    return (
      <nav className="navbar-default navbar-static-side" role="navigation">
        <div className="sidebar-collapse">
          <ul className="nav metismenu" id="side-menu">
            <li className="nav-header">
              <div className="profile-element">
                <img src="/logo.png" style={{height: 80}} />
                <a data-toggle="dropdown" className="dropdown-toggle" href="#">
                  <span className="clear">
                    <span className="text-muted text-xs block" style={{fontSize: 12}} >Messaging simplified.</span>
                  </span>
                </a>
              </div>
            </li>
            <li>
              <Link to={`/`}><i className="fa fa-th-large"></i> <span className="nav-label">Dashboard</span></Link>
            </li>
            <li>
              <a href="#"><i className="fa fa-envelope"></i> <span className="nav-label">Messages</span><span className="fa arrow"></span></a>
              <ul className="nav nav-second-level collapse">
                <li><Link to={`/messages/new`}>New message</Link></li>
              </ul>
            </li>
            <li>
              <a href="#"><i className="fa fa-globe"></i> <span className="nav-label">Campaigns</span><span className="fa arrow"></span></a>
              <ul className="nav nav-second-level collapse">
                <li><Link to={`/campaigns`}>All campaigns</Link></li>
                <li><Link to={`/campaigns/new`}>New campaign</Link></li>
              </ul>
            </li>
            <li>
              <a href="#"><i className="fa fa-users"></i> <span className="nav-label">Receivers</span><span className="fa arrow"></span></a>
              <ul className="nav nav-second-level collapse">
                <li><Link to={`/receivers`}>All receivers</Link></li>
                <li><Link to={`/receivers/new`}>New receiver</Link></li>
              </ul>
            </li>
            <li>
              <a href="#"><i className="fa fa-list"></i> <span className="nav-label">Lists</span><span className="fa arrow"></span></a>
              <ul className="nav nav-second-level collapse">
                <li><Link to={`/lists`}>All lists</Link></li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}
