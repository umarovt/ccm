/**
 * Created by urbanmarovt on 11/02/16.
 */

import React, {Component, PropTypes} from 'react';
import Select from 'react-select';

import styles from './Selects.scss';

export default class SimpleSelect extends Component {

  static propTypes = {
    data: PropTypes.array,
    labelClassName: PropTypes.string,
    wrapperClassName: PropTypes.string,
    label: PropTypes.string,
    onBlur: PropTypes.func,
    touched: PropTypes.bool,
    error: PropTypes.string,
    value: PropTypes.string,
    placeholder: PropTypes.string
  }

  render() {
    const {labelClassName, wrapperClassName, placeholder, data, value, onBlur, label, error, touched, ...props} = this.props;

    return (
      <div className={`${styles.marginBottom} row`}>
        {label &&
        <div className={labelClassName}>
          <label className={`${styles.label} ${touched && error && styles.error}`}>{ label }</label>
        </div>}
        <div className={wrapperClassName}>
          <Select
            className={styles.select}
            value={value}
            onBlur={() => onBlur(value)}
            placeholder={placeholder || ''}
            options = {data}
            {...props} />
          {touched && error &&
          <div className={styles.error}>
            {error}
          </div>}
        </div>
      </div>
    );
  }
}
