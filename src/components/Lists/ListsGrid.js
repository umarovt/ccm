/**
 * Created by rokfortuna on 3/10/16.
 */

import React, {Component, PropTypes} from 'react';
import {Table} from 'react-bootstrap';
import {DateLabel} from 'components';

export default class PackagesGrid extends Component {
  static propTypes = {
    lists: PropTypes.array,
    onPackageClick: PropTypes.func,
    paginationOnPageChange: PropTypes.func,
    paginationPages: PropTypes.number,
    paginationActivePage: PropTypes.number
  };

  render() {
    const {lists, onListClick, paginationOnPageChange, paginationPages, paginationActivePage} = this.props;
    return (
      <div style={{height: '500px'}}>
        <Table className="table" hover>
          <thead>
          <tr>
            <th>
              <strong>Name</strong>
            </th>
            <th>
              <strong>Number of receivers</strong>
            </th>
            <th>
              Tags
            </th>
          </tr>
          </thead>
          <tbody>
          { lists.map(
            (list) =>
              <tr key={list.id} onClick={()=> onListClick(list.id)} className="clickable">
                <td>{list.name}</td>
                <td>{list.numReceivers}</td>
                <td> tag </td>
              </tr>
          )
          }
          </tbody>
        </Table>
      </div>
    );
  }
}
