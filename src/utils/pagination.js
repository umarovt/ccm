/**
 * Created by rokfortuna on 2/15/16.
 */

/* jshint -W065 */

import _ from 'lodash';

export const getLimitOffsetURL = (router, defaultLimit, defaultOffset) => {
  let limit = defaultLimit;
  let offset = defaultOffset;
  if (_.has(router, 'location.query.limit')) {
    limit = router.location.query.limit;
  }
  if (_.has(router, 'location.query.offset')) {
    offset = router.location.query.offset;
  }
  return {limit, offset};
};

export const getOffset = (page, limit) => {
  return ((parseInt(page) - 1) * parseInt(limit));
};

export const getPagesCount = (count, limit) => {
  return Math.ceil(parseInt(count) / parseInt(limit));
};

export const getActivePage = (limit, offset) => {
  return Math.floor(parseInt(offset) / parseInt(limit)) + 1;
};
