/**
 * Created by urbanmarovt on 23/02/16.
 */

import _ from 'lodash';
import {empty} from 'utils/validation';

export const getSearchString = (router) => {
  let searchString = '';
  if (_.has(router, 'location.query.searchString')) {
    searchString = router.location.query.searchString;
  }
  return searchString;
};

export const getSearchTags = (router) => {
  let searchTags = '';
  if (_.has(router, 'location.query.searchTags')) {
    searchTags = router.location.query.searchTags;
  }
  return searchTags;
};

export const setQueryParams = (path, params) => {
  const queryParams = Object.keys(params).reduce((previous, key) => {
    if (empty(params[key])) {
      return previous;
    }
    return `${previous}${key}=${params[key]}&`;
  }, '').slice(0, -1);

  window.history.pushState('', '', `${path}?${queryParams}`);
};

export const getQueryParams = (router, defaultQueryParams) => {
  const queryParams = {...defaultQueryParams};

  if (_.has(router, 'location.query.searchString')) {
    queryParams.searchString = router.location.query.searchString;
  }

  if (_.has(router, 'location.query.limit')) {
    queryParams.limit = router.location.query.limit;
  }

  if (_.has(router, 'location.query.offset')) {
    queryParams.offset = router.location.query.offset;
  }

  if (_.has(router, 'location.query.order')) {
    queryParams.order = router.location.query.order;
  }

  return queryParams;

};

export const toggleOrder = (field, previousOrder) => {
  const split = previousOrder.split(' ');
  if (split[0] === field) {
    return `${field} ${split[1] === 'ASC' ? 'DESC' : 'ASC' }`;
  }
  return `${field} ASC`;
};