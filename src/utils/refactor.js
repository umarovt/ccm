const ChannelType = ['EMAIL', 'SMS', 'PUSH_NOTIFICATION'];
const DeliveryStatus = ['PENDING', 'DELIVERED', 'BOUNCED', 'CONFIRMED_BY_USER', 'DISABLED_BY_USER', 'PROVIDER_ERROR', 'ADDRESS_UNKNOWN'];

export const refactorTags = (tags) => {
  return tags.split(',')
    .filter((tagId)=> {return tagId !== '';})
    .map((tagId) => {return { id: tagId };});
};

export const refactorWebsocketLiveData = (ratesCpy, barsCpy, rdata, rate) => {

  // remove spaces
  for (let i = 0; i < ratesCpy.length; i++) {
    ratesCpy[i][0] = ratesCpy[i][0].replace(/ /g, '_');
  }
  for (let i = 0; i < barsCpy.length; i++) {
    barsCpy[i][0] = barsCpy[i][0].replace(/ /g, '_');
  }

  // bars
  for (let i = 0; i < ChannelType.length; i++) {
    for (let j = 0; j < DeliveryStatus.length; j++) {
      let value = rdata[ChannelType[i]][DeliveryStatus[j]];
      if (typeof value === 'undefined') {
        value = 0;
      }
      barsCpy[j][i + 1] = value;
    }
  }

  // rate
  if (typeof rate !== 'undefined') {
    for (let i = 0; i < ChannelType.length; i++) {
      const channel = ChannelType[i];
      let value = rate[channel];
      if (typeof value === 'undefined') {
        value = 0;
      }

      for (let j = 1; j < ratesCpy.length; j++) {
        if (ratesCpy[j][0] === channel) {
          ratesCpy[j].push(value);
          break;
        }
      }
    }
    ratesCpy[0].push(new Date());
  }

  for (let i = 0; i < ratesCpy.length; i++) {
    // live rates only for past 30 seconds
    if (ratesCpy[i].length > 31) {
      ratesCpy[i] = [ratesCpy[i][0], ...ratesCpy[i].slice(-30)];
    }
  }

  // add spaces back
  for (let i = 0; i < ratesCpy.length; i++) {
    ratesCpy[i][0] = ratesCpy[i][0].replace(/_/g, ' ');
  }
  for (let i = 0; i < barsCpy.length; i++) {
    barsCpy[i][0] = barsCpy[i][0].replace(/_/g, ' ');
  }

  return {ratesNew: ratesCpy, barsNew: barsCpy};
};

export const refactorReceiverPrefs = (receiverPrefs) => {
  const channels = ['SMS', 'EMAIL', 'PUSH_NOTIFICATION'];

  channels.forEach((channel) => {

    const channelPrefs = receiverPrefs.find((preference) => {return preference.channelType === channel;});

    if (channelPrefs === undefined) {
      receiverPrefs.push({channelType: channel, notDefined: true});
    }

  });

  return receiverPrefs;
};
