import moment from 'moment';

export function dateFromTimestamp(timestamp) {
  return moment(timestamp).format('DD.MM.YYYY');
}

export function timeFromTimestamp(timestamp) {
  return moment(timestamp).format('HH:mm');
}

export function timestampFromDate(date) {
  return moment(date, 'DD.MM.YYYY').valueOf();
}

export function postgresDate(momentDate) {
  return momentDate.format('YYYY-MM-DD');
}

export function postgresDateTime(momentDate) {
  return momentDate.format('YYYY-MM-DD HH:mm:ss');
}