export const refactorTags = (tags) => {
  return tags.map(
    (element) => {
      return {label: element.value, value: element.id, color: element.color };
    }
  );
};

export const refactorReceivers = (receivers) => {
  return receivers.map(
    (element) => {
      return {label: `${element.firstName} ${element.lastName}, ${element.email}`, value: element.id
      };
    }
  );
};

export const refactorCampaigns = (campaigns) => {
  return campaigns.map(
    (element) => {
      return {label: element.name, value: element.id };
    }
  );
};
