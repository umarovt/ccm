import React from 'react';
import {IndexRoute, Route} from 'react-router';
import {
    App,

    NotFound,
    NotAuthorized,

    Campaigns,
    CampaignNew,
    Campaign,
    CampaignBasic,
    CampaignStatistics,

    Message,
    MessageNew,

    Receiver,
    ReceiverNew,
    Receivers,
    ReceiverPrefsNew,

    Dashboard,
    DashboardLive,
    DashboardHistory,

    Package,

    Lists,
    List

  } from 'containers';

export default () => {

  /**
   * Please keep routes in alphabetical order!!!
   */
  return (

    <Route path="/" component={App}>
      { /* Home (main) route */ }
      <Route component={Dashboard}>
        <IndexRoute component={DashboardLive}/>
        <Route path="history" component={DashboardHistory} />
      </Route>


      <Route path="/campaigns" component={Campaigns}/>
      <Route path="/campaigns/new" component={CampaignNew}/>
      <Route path="/campaigns/:campaignId" component={Campaign}>
        <IndexRoute component={CampaignBasic} />
        <Route path="statistics" component={CampaignStatistics} />
      </Route>

      <Route path="/messages/new" component={MessageNew}/>
      <Route path="/messages/:messageId" component={Message}/>

      <Route path="/receivers" component={Receivers} />
      <Route path="/receivers/new" component={ReceiverNew} />
      <Route path="/receivers/:receiverId" component={Receiver}/>
      <Route path="/receivers/:receiverId/preferences/new" component={ReceiverPrefsNew} />

      <Route path="/packages/:packageId" component={Package}/>

      <Route path="/lists" component={Lists}/>
      <Route path="/lists/:listId" component={List}/>

      { /* not authorized */ }
      <Route path="/notAuthorized" component={NotAuthorized} status={401} />

      { /* Catch all route */ }
      <Route path="*" component={NotFound} status={404} />

    </Route>

  );
};
